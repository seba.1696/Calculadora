/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Calculadora de operaciones basicas.
 * @version 0.0.1
 * @author Sebastian Gonzalez
 */
public class Calculadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Menu(); //Llamada al menu para inciar programa.
    }

    /**
     * Permite elegir una operacion a realizar.
     */
    public static void Menu() {
        int op = 0; // valor de opcion inicia en cero.
        int a,b;
        do {
            System.out.println("1.Suma");
            System.out.println("2.Resta");
            System.out.println("3.Multiplicacion");
            System.out.println("4.Division");
            System.out.println("5.Salir");
            System.out.println("-------------------");
            op = Leer();
            switch (op) {
                case 1: {
                    System.out.println("Opcion suma: ");
                    a = Leer();
                    b = Leer();
                    System.out.println("La suma es: " + Suma(a, b));
                }
                break;
                case 2: {
                    System.out.println("Opcion resta: ");
                    a = Leer();
                    b = Leer();
                    System.out.println("La resta es: " + Resta(a, b));
                }
                break;
                case 3: {
                    System.out.println("Opcion multiplicacion: ");
                    a = Leer();
                    b = Leer();
                    System.out.println("La multiplicacion es: " + Multiplicacion(a, b));
                }
                break;
                case 4: {
                    System.out.println("Opcion divicion: ");
                    a = Leer();
                    b = Leer();
                    System.out.println("La divicion es: " + Divicion(a, b));
                }
                break;
                case 5: {
                    System.out.println("Adios ");
                }
            }
        } while (op != 5);
    }

    /**
     * Realiza suma de dos numeros.
     * @param a primer valor ingresado.
     * @param b primer valor ingresado.
     * @return suma de dos numeros.
     */
    public static int Suma(int a, int b) {
        int suma;
        suma = a + b; //operacion.
        return suma;
    }

    /**
     * Realiza resta de dos numeros.
     * @param a primer valor ingresado.
     * @param b segundo valor ingresado.
     * @return resta de dos numeros.
     */
    public static int Resta(int a, int b) {
        int resta;
        resta = a - b; //operacion.
        return resta;
    }

    /**
     * Realiza multiplicacion de dos numeros.
     * @param a primer valor ingresado.
     * @param b segundo valor ingresado.
     * @return multiplicacion de dos numeros.
     */
    public static int Multiplicacion(int a, int b) {
        int multiplicacion;
        multiplicacion = a * b; //operacion.
        return multiplicacion;
    }

    /**
     * Realiza division de dos numeros.
     * @param a primer valor ingresado.
     * @param b segundo valor ingresado.
     * @return division de dos numeros.
     */
    public static float Divicion(int a, int b) {
        float divicion;
        divicion = (float) a / b; //operacion.
        return divicion;
    }

    /**
     * Comprueba que el valor ingresado sea entero.
     * @return valor ingresado si es un entero.
     */
    public static int Leer() {
        int op = 0;
        boolean valor;
        BufferedReader leer = new BufferedReader(new InputStreamReader(System.in));
        do {
            try {
                op = Integer.parseInt(leer.readLine());
                valor = true;

            } catch (Exception e) {
                System.out.println("Ingrese solo numeros.");
                System.out.println("------------------------------------------");
                valor = false;
            }
        } while (valor == false);
        return op;
    }

}
